function filterdependantDropdown(filteringField,filteredFieldName) {
	//debugger;
	var filteredField = document.getElementById("edit-"+filteredFieldName+"-keys");
	if (! (filteredField.parentNode.backup)) {
		// initialize
		var filterDiv = document.getElementById(filteredFieldName + "_filtervalues");
		filteredField.parentNode.filterArray = Drupal.parseJson(filterDiv.innerHTML);
		
		var backupNode =  filteredField.cloneNode(true);
		backupNode.name = "backup";
		backupNode.style.display = "none";
		filteredField.parentNode.backup = backupNode;
	} else {
		var backupNode = filteredField.parentNode.backup;
		var newFilteredField = backupNode.cloneNode(true);
		newFilteredField.id = "edit-"+filteredFieldName+"-keys";
		newFilteredField.style.display = "block";
		filteredField.parentNode.replaceChild(newFilteredField,filteredField);		
		filteredField = newFilteredField;
	}
	
	
	for (var i=0;i<filteredField.childNodes.length;i++) {
		filteredField.childNodes[i].isToBeRemoved = (filteredField.parentNode.filterArray[i] != filteringField.getSelectedText());
	}
	for (var i=0;i<filteredField.childNodes.length;i++) {
		if (filteredField.childNodes[i].isToBeRemoved) {
			filteredField.removeChild(filteredField.childNodes[i]);
			i = -1;
		}
	}

}

function dependantDropdownAutoAttach() {
	//debugger;
	
  	$('select.filteringField').each(function () {
			this.filteredName = this.title;
			this.onchange = function () {
					filterdependantDropdown(this,this.filteredName);
				}
			this.getSelectedText = function () {
				for (var i=0;i<this.childNodes.length;i++) {
					if (this.value == this.childNodes[i].value) {
						return this.childNodes[i].innerHTML;
					}
				}
			}
			this.removeAttribute('title');
			this.onchange();
		} )
}
  
if (Drupal.jsEnabled) {
  $(document).ready(dependantDropdownAutoAttach);
}